// A dead-simple implementation of ESI:include.
// Replaces all <esi:include> elements with the contents of the response to
// their <tt>src</tt> attribute.
//
// Given: <esi:include src="/dingo" />
// Requests: GET /dingo
// Receives: "<div>wah wah</div>"
// Replaces esi:include with that.
(function ($) {

$.fn.esiIncludeFetch = function() {
  var self = this;
  jQuery.ajax({
    url: self.attr("src"),
    type: "GET",
    dataType: "html",
    complete: function(res, status){
      if (status == "success" || status == "notmodified") {
        self.replaceWith(res.responseText);
      }
    }
  });
  return this;
};

$(function(){

  includeElements = document.getElementsByTagName("esi:include");
  for (i = 0; i < includeElements.length; i++) {
    $(includeElements[i]).esiIncludeFetch();
  }

  removeElements = document.getElementsByTagName("esi:remove");
  $(removeElements).remove();

});

})(jQuery);